import Home from "./pages/Home";
import SportEvent from "./pages/SportEvent";
import Outcome from "./pages/Outcome";
import NotFound from "./pages/NotFound";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/sports" element={<Home />} />
        <Route exact path="/sports/:sportId/events" element={<SportEvent />} />
        <Route
          exact
          path="/sports/:sportId/events/:eventId/outcomes"
          element={<Outcome />}
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
}

export default App;
