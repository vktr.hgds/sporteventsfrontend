import styled from "styled-components";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <Container>
      <Wrapper>
        <Center>
          <Link to="/sports" style={{ textDecoration: "none" }}>
            <Logo color="#6cd6f7">MY</Logo>
            <Logo color="#8f8f8f">PAGE</Logo>
          </Link>
        </Center>
      </Wrapper>
    </Container>
  );
};

export default Navbar;

const mainBgColor = "#1c1f25";

const Container = styled.div`
  height: 70px;
  background-color: ${mainBgColor};
  position: sticky;
  position: -webkit-sticky;
  top: 0;
  bottom: 0;
  left: 0;
  z-index: 4;
  transition: all 0.3s ease-in-out;
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const Center = styled.div`
  flex: 1;
  text-align: center;
`;

const Logo = styled.span`
  font-weight: bold;
  text-decoration: none;
  color: ${(props) => props.color};
  font-size: 32px;

  @media screen and (max-width: 400px) {
    font-size: 24px;
  }
`;
