import { SearchOutlined } from "@material-ui/icons";

import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const SportItem = ({ item }) => {
  return (
    <Container>
      <Rectangle>
        <RectangleInfo>
          {item?.name} ({item?.eventCount})
        </RectangleInfo>
      </Rectangle>
      <Info>
        <Icon color="#22415e" bg="#96cadd">
          <Link
            to={`/sports/${item?.id}/events`}
            style={{ textDecoration: "none" }}
          >
            <SearchOutlined style={{ textDecoration: "none" }} />
          </Link>
        </Icon>
      </Info>
    </Container>
  );
};

const Container = styled.div`
  flex: 1;
  margin: 7px;
  min-width: 280px;
  height: 350px;
  background-color: #e6edf0;
  border-radius: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  border: 1px solid #a1bfca;
  transition: all 0.4s ease;

  &:hover {
    border: 1px solid #ffffff;
  }
`;

const Rectangle = styled.div`
  width: 99%;
  height: 99%;
  border-radius: 20px;
  background-color: #ffffff;
  position: absolute;
  z-index: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const RectangleInfo = styled.span`
  font-size: 40px;
`;

const Info = styled.div`
  opacity: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(78, 78, 78, 0.3);
  border-radius: 20px;
  z-index: 2;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.4s ease;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }
`;

const Icon = styled.div`
  width: 35px;
  height: 35px;
  margin: 0 15px;
  padding: 10px;
  background-color: #ffffff;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.3s ease;
  cursor: pointer;

  &:hover,
  &:active {
    color: ${(props) => props.color};
    background-color: ${(props) => props.bg};
    transform: scale(1.16);
    box-shadow: 3px 3px rgba(0, 0, 0, 0.1);
  }
`;

export default SportItem;
