import { GitHub, MailOutline, Home, AccountCircle } from "@material-ui/icons";
import styled from "styled-components";

const Footer = () => {
  return (
    <Container>
      <Title>Contact</Title>
      <Center>
        <ContactItem>
          <AccountCircle style={{ marginRight: "10px" }} /> Viktor Hegedűs
        </ContactItem>
        <ContactItem>
          <Home style={{ marginRight: "10px" }} /> Budapest, Hungary
        </ContactItem>
        <ContactItem>
          <MailOutline style={{ marginRight: "10px" }} />{" "}
          viktorhegedus96@gmail.com
        </ContactItem>
        <ContactItem>
          <GitHub style={{ marginRight: "10px" }} />{" "}
          https://gitlab.com/vktr.hgds
        </ContactItem>
      </Center>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 20px;
  background-color: #f3f3f3;
  border-top: 1px solid #e4e4e4;
`;

const Title = styled.h3`
  padding-left: 20px;
  width: 100%;
`;

const Center = styled.div`
  display: flex;
  padding-top: 30px;
  flex: 1;
  flex-direction: row;
  padding: 20px;

  @media screen and (max-width: 1200px) {
    flex-direction: column;
  }
`;

const ContactItem = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 10px;
  width: 25%;

  @media screen and (max-width: 1200px) {
    width: 100%;
  }
`;

export default Footer;
