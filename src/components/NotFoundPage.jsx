import React from "react";
import styled from "styled-components";
import { SentimentVeryDissatisfiedSharp } from "@material-ui/icons";

const NotFoundPage = () => {
  return (
    <Container>
      <NotFoundStatus>
        404
        <SentimentVeryDissatisfiedSharp
          style={{ fontSize: "40px", paddingLeft: "20px" }}
        />
      </NotFoundStatus>
      <NotFoundMessage>This page could not be found.</NotFoundMessage>
    </Container>
  );
};

const Container = styled.div`
  width: 100vw;
  height: 70vh;
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const NotFoundStatus = styled.span`
  font-size: 60px;
  padding: 20px;
`;

const NotFoundMessage = styled.span`
  font-size: 40px;
`;

export default NotFoundPage;
