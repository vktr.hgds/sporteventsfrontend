import styled from "styled-components";
import React, { useEffect, useState } from "react";
import Aos from "aos";
import "aos/dist/aos.css";

import EventItem from "./EventItem";
import axios from "axios";
import { useParams } from "react-router";

const Events = ({ title }) => {
  const [events, setEvents] = useState([]);
  const [titleSport, setTitleSport] = useState("");
  const { sportId } = useParams();

  // effect/animation
  useEffect(() => {
    Aos.init({
      mirror: false,
    });
  }, []);

  // get events by backend API
  useEffect(() => {
    const getEvents = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3000/api/v1/sports/${sportId}/events`
        );
        setEvents(response.data);
        setTitleSport(response.data[0]?.event?.sportName);
      } catch (err) {}
    };

    getEvents();
  }, []);

  return (
    <Container>
      {title && (
        <InfoContainer>
          <Info data-aos="fade-up-right" data-aos-duration="1000">
            {title} {titleSport ? `for ${titleSport}` : ""}
            <Border data-aos="fade-down-right" data-aos-duration="300" />
          </Info>
        </InfoContainer>
      )}
      <Wrapper data-aos="fade-left" data-aos-duration="1100">
        {events.map((item) => (
          <EventItem item={item} key={item.id} sportId={sportId} />
        ))}
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const InfoContainer = styled.div`
  width: 100%;
  padding: 20px 0px 40px 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const Info = styled.span`
  letter-spacing: 2px;
  position: relative;
  font-size: 40px;
  font-weight: 500;
  padding-bottom: 8px;

  @media screen and (max-width: 500px) {
    font-size: 25px;
  }
`;

const Border = styled.div`
  border-bottom: 3px solid #529eb6;
  position: absolute;
  left: 50px;
  right: 50px;
  bottom: 0;
`;

export default Events;
