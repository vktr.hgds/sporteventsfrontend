import React from "react";
import styled from "styled-components";

const OutcomeItem = ({ item }) => {
  return (
    <Container>
      <Rectangle>
        <RectangleWinner>{item?.competitors?.winner}</RectangleWinner>
        <RectangleInfo color="#a52828">
          {item?.competitors?.homeCompetitor} {item?.competitors?.homeScore}
        </RectangleInfo>
        <RectangleInfo bottomPadding="20px" color="#0c9397">
          {item?.competitors?.awayCompetitor} {item?.competitors?.awayScore}
        </RectangleInfo>
        <RectangleDate>Start at: {item?.startDate}</RectangleDate>
        <RectangleDate>Ends at: {item?.endDate}</RectangleDate>
        <RectangleCompetition>
          {item?.event?.sportName}, {item?.event?.leagueName},{" "}
          {item?.event?.roundName}
        </RectangleCompetition>
      </Rectangle>
    </Container>
  );
};

const Container = styled.div`
  flex: 1;
  margin: 7px;
  min-width: 280px;
  height: 350px;
  background-color: #e6edf0;
  border-radius: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  border: 1px solid #a1bfca;
  transition: all 0.4s ease;

  &:hover {
    border: 1px solid #ffffff;
  }
`;

const Rectangle = styled.div`
  width: 99%;
  height: 99%;
  border-radius: 20px;
  background-color: #ffffff;
  position: absolute;
  z-index: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const RectangleWinner = styled.span`
  font-size: 30px;
  display: flex;
  padding: 30px;
  color: #559b1d;
  font-weight: 700;

  @media screen and (max-width: 600px) {
    font-size: 15px;
  }
`;

const RectangleInfo = styled.span`
  font-size: 35px;
  padding-bottom: ${(props) =>
    props.bottomPadding ? props.bottomPadding : "0px"};

  color: ${(props) => (props.color ? props.color : "#555555")};

  @media screen and (max-width: 600px) {
    font-size: 17px;
  }
`;

const RectangleDate = styled.span`
  font-size: 14px;

  @media screen and (max-width: 1200px) {
    font-size: 11px;
  }
`;

const RectangleCompetition = styled.span`
  font-size: 14px;
  display: flex;
  padding-top: 30px;

  @media screen and (max-width: 1600px) {
    font-size: 11px;
  }
`;

export default OutcomeItem;
