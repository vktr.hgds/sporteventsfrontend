import styled from "styled-components";
import React, { useEffect, useState } from "react";
import Aos from "aos";
import "aos/dist/aos.css";

import SportItem from "./SportItem";
import axios from "axios";

const Sports = ({ title }) => {
  const [sports, setSports] = useState([]);
  const [pos, setPos] = useState("asc");

  // effect/animation
  useEffect(() => {
    Aos.init({
      mirror: false,
    });
  }, []);

  // get sports by backend API
  useEffect(() => {
    const getSports = async () => {
      try {
        const posParam = pos ? `?pos=${pos}` : "";
        const response = await axios.get(
          `http://localhost:3000/api/v1/sports${posParam}`
        );
        setSports(response.data);
      } catch (err) {}
    };

    getSports();
  }, [pos]);

  // set position
  const handlePos = (e) => {
    setPos(e.target.value);
  };

  return (
    <Container>
      {title && (
        <InfoContainer>
          <Info data-aos="fade-up-right" data-aos-duration="1000">
            {title}
            <Border data-aos="fade-down-right" data-aos-duration="1300" />
          </Info>
        </InfoContainer>
      )}

      <FilterContainer>
        <Filter>
          <FilterText>Sort by position:</FilterText>
          <Select onChange={handlePos}>
            <Option defaultValue value="asc">
              Ascending
            </Option>
            <Option value="desc">Descending</Option>
          </Select>
        </Filter>
      </FilterContainer>

      <Wrapper data-aos="fade-left" data-aos-duration="1100">
        {sports.map((item) => (
          <SportItem item={item} key={item.id} />
        ))}
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const InfoContainer = styled.div`
  width: 100%;
  padding: 20px 0px 40px 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const FilterContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Filter = styled.div`
  margin: 20px;
`;

const FilterText = styled.span`
  font-size: 20px;
  font-weight: 600;
  margin-right: 20px;

  @media screen and (max-width: 600px) {
    font-size: 12px;
  }
`;

const Select = styled.select`
  margin-right: 20px;
  padding: 10px;

  &:focus {
    text-decoration: none;
  }

  @media screen and (max-width: 600px) {
    padding: 1px;
    font-size: 12px;
  }
`;

const Option = styled.option`
  margin: 20px;
`;

const Info = styled.span`
  letter-spacing: 2px;
  position: relative;
  font-size: 40px;
  font-weight: 500;
  padding-bottom: 8px;

  @media screen and (max-width: 500px) {
    font-size: 25px;
  }
`;

const Border = styled.div`
  border-bottom: 3px solid #529eb6;
  position: absolute;
  left: 50px;
  right: 50px;
  bottom: 0;
`;

export default Sports;
