import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Outcomes from "../components/Outcomes";

const Outcome = () => {
  return (
    <>
      <Navbar />
      <Outcomes title="Matches" />
      <Footer />
    </>
  );
};

export default Outcome;
