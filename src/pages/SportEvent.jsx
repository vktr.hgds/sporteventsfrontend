import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Events from "../components/Events";

const SportEvent = () => {
  return (
    <>
      <Navbar />
      <Events title="Events" />
      <Footer />
    </>
  );
};

export default SportEvent;
