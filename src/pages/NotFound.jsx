import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import NotFoundPage from "../components/NotFoundPage";

const NotFound = () => {
  return (
    <>
      <Navbar />
      <NotFoundPage />
      <Footer />
    </>
  );
};

export default NotFound;
