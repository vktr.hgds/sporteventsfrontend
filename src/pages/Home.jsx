import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Sports from "../components/Sports";

const Home = () => {
  return (
    <>
      <Navbar />
      <Sports title="Sports for betting" />
      <Footer />
    </>
  );
};

export default Home;
